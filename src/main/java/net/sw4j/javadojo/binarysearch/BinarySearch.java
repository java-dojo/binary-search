package net.sw4j.javadojo.binarysearch;

/**
 * A class to perform a binary search on a sorted array of {@code int}s.
 */
public class BinarySearch implements Search {

    /**
     * Searches the value {@code toSearch} in the given (sorted) array of
     * {@code values}.
     *
     * @param values the sorted array to search through.
     * @param toSearch the value to search.
     * @return {@code true} if the value {@code toSearch} is found in the array
     *  {@code values}.
     */
    @Override
    public boolean search(final int[] values, final int toSearch) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
